program ManifestWiper;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, Windows;

procedure RemoveManifest(const filepath: String);
var
  handle: THANDLE;
  rt_manifest, sxs_id: Pointer;
  lang: word;
  result: boolean;
begin
  rt_manifest := MakeIntResource(24);

  if (StrPos(PChar(filepath), PChar('.dll')) <> nil)
    then sxs_id := MakeIntResource(2)
    else sxs_id := MakeIntResource(1);

  //lang := (SUBLANG_ENGLISH_US shl 10) or LANG_ENGLISH;
  lang := 1033;

  handle := BeginUpdateResource(PChar(filepath), False);
  if (handle <> 0) then begin
    result := UpdateResource(handle, rt_manifest, sxs_id, lang, nil, 0);
    if result then begin
      if EndUpdateResource(handle, False) then begin
        WriteLn('Removed SxS manifest from "' + filepath + '".');
      end
      else begin
        RaiseLastOSError();
      end;
    end
    else begin
      RaiseLastOSError();
    end;
  end
  else begin
    raise Exception.Create('Unable to open file ' + filepath);
  end;
end;

var
  i: integer;

{$R *.res}

begin
  if (ParamCount <= 0) then begin
    WriteLn('Nothing to do. Please pass a full path to a exe/dll for manifest wiping.');
  end;

  for i := 1 to ParamCount do begin
    try
      RemoveManifest(ParamStr(i));
    except
      on E: Exception do begin
        WriteLn('Unable to remove manifest from "' + ParamStr(i) +'".');
        halt(i);
      end;
    end;
  end;
end.

